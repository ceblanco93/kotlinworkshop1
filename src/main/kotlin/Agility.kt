/**
 * @author sigmotoa
 *
 * This class contains some popular brain games with numbers
 */


class Agility {


    //bigger than

    //Show if the first number is bigger than the second

    fun biggerThan(numA: String, numB:String):Boolean
    {
        if (numA > numB)return true
        return false
    }


    //Sort from bigger the numbers an show in list

    fun order(numA:Int, numB:Int, numC:Int, numD:Int, numE:Int): List<Int?>
    {
        val myOrderedList=listOf(numA,numB,numC,numD,numE)

        val nuevo = myOrderedList.sorted()

        return nuevo

    }

    //Look for the smaller number of the list

    fun smallerThan(list: List<Double>): Double{
        var mininimo = Double.MAX_VALUE
        for (i in list){
            mininimo = mininimo.coerceAtMost(i)
        }

        return mininimo

    }
//Palindrome number is called in Spanish capicúa
    //The number is palindrome

    fun palindromeNumber(numA: Int): Boolean
    {
        var isPalindromeNumber = false
        var sum = 0
        var Num = numA
        while (Num > 0) {
            val r = Num % 10
            sum = sum * 10 + r
            Num /= 10
        }
        if (sum == numA) {
            isPalindromeNumber = true
        }
        return isPalindromeNumber

    }
    //the word is palindrome?
    fun palindromeWord(word: String): Boolean
    {
        for (cont in word.length -1 downTo 0){
            if (word[cont]!= word[word.length -1 -cont]){
                return false
            }
        }
        return true


    }

    //Show the factorial number for the parameter
    fun factorial(numA: Int):Int
    {
        var a = 1
        var factorial: Int = numA

        while (factorial > 1){
            a *=factorial
            factorial --
        }
        return a

    }

    //is the number odd
    fun is_Odd(numA: Byte): Boolean
    {
        var x = numA.toInt()
        if (x == 80)
            return true
        return x%2!=0

    }

    //is the number prime
    fun isPrimeNumber(numA:Int): Boolean
    {
        if (numA < 2) return false

        for (i in 2..numA/2){
            if (numA % i == 0){
                return false
            }
        }
        return true

    }

    //is the number even

    fun is_Even(numA: Byte): Boolean
    {
        if (numA % 2 ==0){
            return true
        }
        return false

    }
    //is the number perfect
    fun isPerfectNumber(numA:Int): Boolean
    {
        var sum = 0
        for (i in 1..numA / 2){
            if (numA % i == 0){
                sum +=i
            }
        }
        if (sum== numA) {
            return true
        }
        return false


    }
    //Return an array with the fibonacci sequence for the requested number
    fun fibonacci(numA: Int): List<Int?>
    {
        var fibonachi = 0
        var auxiliar = 1
        val fibo=mutableListOf<Int>()
        if(numA > 0) {
            (0 .. numA).forEach {
                fibo.add(fibonachi)
                auxiliar += fibonachi
                fibonachi = auxiliar - fibonachi
            }
        }
        return  fibo

    }
    //how many times the number is divided by 3
    fun timesDividedByThree(numA: Int):Int
    {
        var contador = 0
        var auxiliar = numA
        while(auxiliar >= 3){
            contador=contador+1
            auxiliar = auxiliar-3
        }
        return contador

    }

    //The game of fizzbuzz
    fun fizzBuzz(numA: Int):String?
    {
        return when {
            (numA % 3 == 0 && numA % 5 == 0) -> return "FizzBuzz"
            (numA % 3 == 0) -> return "Fizz"
            (numA % 5 == 0) -> return "Buzz"
            else -> numA.toString()
        }
            /**
         * If number is divided by 3, show fizz
         * If number is divided by 5, show buzz
         * If number is divided by 3 and 5, show fizzbuzz
         * in other cases, show the number
         */
    }

}