/**
 * @author sigmotoa
 *
 * @version 1
 *
 * Convertion Exercises
 */
class Convertion {
    //convert of units of Metric System
    //Km to metters
    fun kmToM1(km: Double): Int {
        var R =0
        R =(km*1000).toInt()
        return R
    }

    //Km to metters
    fun kmTom(km: Double): Double {
        val met=km*1000
        return met
    }

    //Km to cm
    fun kmTocm(km: Double): Double {
         val cm=km*100000

        return cm
    }

    //milimetters to metters
    fun mmTom(mm: Int): Double {
        val tom=mm*0.001
        return tom
    }

    //convert of units of U.S Standard System
    //convert miles to foot
    fun milesToFoot(miles: Double): Double {
        val fo=miles*5280
        return fo
    }

    //convert yards to inches
    fun yardToInch(yard: Int): Int {
        val yardin=yard*(36/10)
        return yardin
    }

    //convert inches to miles
    fun inchToMiles(inch: Double): Double {
        var x=0.0
        x=inch/63360.0
        return x
    }

    //convert foot to yards
    fun footToYard(foot: Int): Int {
        val yards = foot*0.333333
        return yards.toInt()
    }

    //Convert units in both systems
    //convert Km to inches
    fun kmToInch(km: String?): Double {

        val kmtoinches = km?.toDoubleOrNull()
        return (kmtoinches!! * (1000 / 1) * (100 / 1) * (1 / 2.54))
    }

    //convert milimmeters to foots
    fun mmToFoot(mm: String?): Double {

        val mmtofoots = mm?.toDouble()
        return (mmtofoots!! * (1 / 304.8))
    }

    //convert yards to cm
    fun yardToCm(yard: String?): Double {
        var num = yard?.toDouble()
        var  A = (num!! * 91.44)

        return A


    }
}