import kotlin.math.hypot
import kotlin.math.pow
import kotlin.math.sqrt

/**
 * @author sigmotoa
 */

class Geometric {

    //Calculate the area for a square using one side
    fun squareArea(side: Int): Int {

        return side*side
    }

    //Calculate the area for a square using two sides
    fun squareArea(sideA: Int, sideB: Int): Int {

        return sideA*sideB
    }

    //Calculate the area of circle with the radius
    fun circleArea(radius: Double): Double {

        val pi=3.1416
        return 2*pi*(radius*radius)
    }

    //Calculate the perimeter of circle with the diameter
    fun circlePerimeter(diameter: Int): Double {

        val pi=3.1416
        return pi*diameter
    }

    //Calculate the perimeter of square with a side
    fun squarePerimeter(side: Double): Double {

        return 81.0
    }

    //Calculate the volume of the sphere with the radius
    fun sphereVolume(radius: Double): Double {

        val pi=3.1416
        val calculo=Math.pow(radius,3.0)
        val result=(4*pi*calculo)/3
        return result
    }

    //Calculate the area of regular pentagon with one side
    fun pentagonArea(side: Int): Float {
        val result =((sqrt(5.00*(5.00+2.00*(sqrt(5.0).toFloat()))).toFloat()*side.toFloat()*side.toFloat())/4.00).toFloat()
                val re=(Math.round(result*10.0)/10.0).toFloat()
        return re
    }

    //Calculate the Hypotenuse with two legs
    fun calculateHypotenuse(legA: Double, legB: Double): Double {
        return hypot(legA,legB)
    }
}